#include <iostream>

using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente{
    private:
        string nombre;
        string telefono;
        bool estado_Moroso;
        int saldo;

    public:
        Cliente(); 
        string get_nombre();
        string get_telefono();
        bool get_estado_moroso();
        int get_saldo();
        void add_datos_cliente();
        void set_estado_moroso(bool estado_Moroso);

};
#endif

