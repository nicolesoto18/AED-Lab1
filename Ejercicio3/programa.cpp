#include <iostream>
#include <stdio.h>
#include "Cliente.h"

using namespace std;
    
void llenar_arreglo(string num_clientes, Cliente *arreglo_cliente){
    for (int i = 0; i < stoi(num_clientes); i++){
        // Agregar los datos de cada cliente
        arreglo_cliente[i].add_datos_cliente();

        cout << "\n";

        // Limpiar el buffer
        while(getchar()!='\n');     
    }
}


void imprimir_datos(string num_clientes, Cliente *datos_cliente){
    for (int i = 0; i < stoi(num_clientes); i++){
        cout << "\n\n\tDATOS CLIENTE" << endl;
        cout << "  -Nombre: " << datos_cliente[i].get_nombre() << endl;
        cout << "  -Teléfono: " << datos_cliente[i].get_telefono() << endl;
        cout << "  -Saldo de la deuda: " << datos_cliente[i].get_saldo() << endl;
    
        // Mostrar si el cliente es moroso
        if (datos_cliente[i].get_estado_moroso() == true){
            cout << "\n\tCLIENTE MOROSO" << endl;
        }

        else{
            cout << "\n\tCLIENTE SIN DEUDA" << endl;
        }
    }
}


int main(int argc, char *argv[]){
    string num_clientes;
    
    cout << "Ingrese la cantidad de clientes: ";
    getline(cin, num_clientes);

    // Crear arreglo de tipo cliente
    Cliente datos_cliente[stoi(num_clientes)]; 

    llenar_arreglo(num_clientes, datos_cliente);
    imprimir_datos(num_clientes, datos_cliente);

    return 0;

}
