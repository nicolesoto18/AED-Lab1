#include <iostream>
#include "Cliente.h"

using namespace std;

// Constructor por defecto
Cliente::Cliente(){
}


// Setter 
void Cliente::set_estado_moroso(bool estado_Moroso){
    this->estado_Moroso = estado_Moroso;
}

// Añade los datos del cliente
void Cliente::add_datos_cliente(){
    cout << "Ingrese su nombre: ";
    getline(cin, this->nombre);
    
    cout << "¿Cuál es su número de teléfono? ";
    getline(cin, this->telefono);
    
    cout << "Ingrese su saldo: ";
    cin >> this->saldo;
    
    // Define si el cliente es moroso
    // Saldo mayor a cero significa que hay una deuda
    if (this->saldo > 0){
        set_estado_moroso(true);
    }

    else{
        set_estado_moroso(false);
    }
}


// Getters
string Cliente::get_nombre(){
    return this->nombre;
}


string Cliente::get_telefono(){
    return this->telefono;
}


bool Cliente::get_estado_moroso(){
    return this->estado_Moroso;
}


int Cliente::get_saldo(){
    return this->saldo;
}
