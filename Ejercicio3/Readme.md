						    
                                                      	     REGISTRO CLIENTES          
						      ---------------------------------

La creación de este programa esta enfocada en la manipulación de un arreglo de tipo cliente que contendra n cantidad de clientes y cada cliente tendra sus datos.

+ Empezando
    El programa consiste en un registro de datos de los clientes de una empresa, primero se ingresara el número de clientes, luego se debe ingresar el nombre, teléfono y saldo por cada cliente, luego se imprimiran los datos y se determinara si el clientes es moroso o no.

+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa
	     

+ Autor
    Nicole Soto.




