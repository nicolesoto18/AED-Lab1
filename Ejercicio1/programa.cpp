#include <iostream>
#include "Operaciones.h"

using namespace std;

int main(int argc, char *argv[]){
    int tamano;
    int numeros[tamano];
    int resultado;

    // El tamaño del arreglo se ingresa por parámetros
    tamano = stoi(argv[1]);
    
    // Se crea el objeto
    Operaciones suma = Operaciones(tamano);
    suma.LlenarArreglo();
    suma.SumarCuadrados();

    cout << "\nEl resultado de la suma al cuadrado es: " << suma.getResultado() << endl;

    return 0;
}
    
