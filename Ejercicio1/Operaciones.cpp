#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Operaciones.h"

using namespace std;

Operaciones::Operaciones(int tamano){
    this->tamano = tamano;
    this->numeros = new int [this->tamano];

}


void Operaciones::LlenarArreglo(){
    // Se crean aleatoriamente los números para cada posición
    for (int i = 0; i < this->tamano; i++){
        this->numeros[i] = rand()%10;
        cout << "Número " << i + 1 << ": " << numeros[i] << endl;
    }
}


void Operaciones::SumarCuadrados(){
    int suma = 0;
    
    // Suma los elementos del arreglo
    for (int i = 0; i < this->tamano; i++){
        suma += this->numeros[i];
    }
    
    // Eleva la suma al cuadrado
    this->resultado = suma * suma;
}


int Operaciones::getResultado(){
    return this->resultado;
}
 
