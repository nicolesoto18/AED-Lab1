#include <iostream>

using namespace std;
 
#ifndef OPERACIONES_H
#define OPERACIONES_H

class Operaciones{
    private:
        int tamano;
        int *numeros = NULL;
        int resultado;

    public:
        Operaciones(int tamano);
        void LlenarArreglo();
        void SumarCuadrados();
        int getResultado();
};
#endif

