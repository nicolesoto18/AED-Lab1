						    
                                                            SUMA DE CUADRADOS          
							  ---------------------

La creación de este programa esta enfocada en la manipulación de un arreglo unidimensional de números enteros y la realización de operaciones con los elementos de este.

+ Empezando
    El programa cosiste en una "cálculadora" de números aleatorios, primero se generara una cantidad de números aleatorios dependiendo del tamaño del arreglo, este es ingresado por el usuario como parámetro por terminal, luego los números seran sumados y el resultado sera elevado al cuadrado.

+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa parámetro.
	    Parámetro --> Largo del arreglo.
        

+ Autor
    Nicole Soto.




