#include <iostream>
#include "Contar_letras.h"

using namespace std;

// Constructor
Contar_letras::Contar_letras(int tamano){
    this->tamano = tamano;
    this->frase = new string [this->tamano];
}

// Se agrega la frase al arreglo
void Contar_letras::llenar_arreglo(){
    char letra;
    int aux = 0;

    cout << "Ingrese una frase de " << this->tamano << " letras" << endl;
    
    // Se agregaran letras hasta que se alcance el tamaño ingresado
    // Sin contar espacios vacios
    while(aux < this->tamano){
        cin >> letra;
        // Se agrega una letra por cada posición
        this->frase[aux] = letra;
        aux++;
    }
}


void Contar_letras::cantidad_minusculas_mayusculas(){
    for (int i = 0; i < this->tamano; i++){
        cout << this->frase[i] << endl;

        // Verifica si la letra es minuscula o mayuscula
        if(this->frase[i] >= "a" && this->frase[i] <= "z"){
            minusculas++;
        }

        else if(this->frase[i] >= "A" && this->frase[i] <= "Z"){
	        mayusculas++;
        }
    }
}
 

// Getters
int Contar_letras::get_minusculas(){
    return this->minusculas;
}


int Contar_letras::get_mayusculas(){
    return this->mayusculas;
}
