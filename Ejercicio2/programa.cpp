#include <iostream>
#include "Contar_letras.h"

using namespace std; 

int main(int argc, char *argv[]){
    int tamano = stoi(argv[1]);

    cout << "\n\tCONTADOR DE MAYUSCULAS \n \t\t  Y \n \t      MINUSCULAS\n" << endl;
    
    // Se crea el objeto
    Contar_letras cantidad_letras = Contar_letras(tamano);
    cantidad_letras.llenar_arreglo();
    cantidad_letras.cantidad_minusculas_mayusculas();
    
    cout << "\nSu frase tiene: " << endl;
    cout << "  - Minusculas: " << cantidad_letras.get_minusculas() << endl;
    cout << "  - Mayúsculas: " << cantidad_letras.get_mayusculas() << endl;

    return 0;
}
    

