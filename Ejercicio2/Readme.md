						    
                                                      CONTADOR DE MINUSCULAS Y MAYUSCULAS          
						-----------------------------------------------

La creación de este programa esta enfocada en la manipulación de un arreglo unidimensional de tipo string y el conteo de sus elementos.

+ Empezando
    El programa consiste en un contador para letras mayúsculas y minúsculas, primero se ingresara el número de letras que tiene la frase, luego se debe ingresar una frase a la cual se le contara la cantidad de mayúsculas y minúsculas que la componen.

+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./contarLetras parámetro.
	     Parámetro --> Número de letras que tiene la frase sin contar espacios vacios.


+ Autor
    Nicole Soto.




