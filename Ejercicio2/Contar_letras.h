#include <iostream>

using namespace std;

#ifndef CONTAR_LETRAS_H
#define CONTAR_LETRAS_H

class Contar_letras{
    private:
        int tamano;
        int mayusculas = 0;
        int minusculas = 0;
        string *frase = NULL;
    
    public:
        Contar_letras(int tamano);
        void llenar_arreglo();
        void cantidad_minusculas_mayusculas();
        int get_minusculas();
        int get_mayusculas();
        
};
#endif
        
